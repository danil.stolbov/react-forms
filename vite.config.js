import path from 'path';
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

const mode = process.env.NODE_ENV;
const isProd = mode === 'production';
const config = {
  mode,
  resolve: {
    alias: {
      '@types': path.resolve(__dirname, './src/types')
    }
  },
  build: {
    outDir: path.resolve(__dirname, 'dist'),
    sourcemap: isProd,
    emptyOutDir: isProd,
    lib: {
      entry: path.resolve(__dirname, 'packages/react-like/index.ts'),
      name: '[name]',
      fileName: ext => `[name]/[name].${ext}.js`,
      formats: ['es', 'cjs']
    },
    rollupOptions: {
      input: {
        react: path.resolve(__dirname, 'src')
      },
      output: {
        assetFileNames: assetInfo => {
          let [, ext] = assetInfo.name.split('.');

          if ((/png|jpe?g|svg|gif/i).test(ext)) {
            ext = 'img';
          }
          return `${ext}/[name][hash][extname]`;
        }
      }
    },
    plugins: [
      react({
        babel: {
          plugins: [
            ['@babel/plugin-proposal-decorators', { 'legacy': true }],
            ['@babel/plugin-proposal-class-properties']
          ]
        }
      })
    ]
  }
};

export default defineConfig(config);
